TARGET=ztxtool
CC=gcc
CCFLAGS=-c -std=c90 -O2
LD=gcc
LDFLAGS=-lpng -lz -lSDL2 -lGL
RM=rm -rf
OBJS=	main.o \
	ztx.o

all: $(TARGET)

$(TARGET): $(OBJS)
	$(LD) $(OBJS) -o $(TARGET) $(LDFLAGS)

main.o: main.c
	$(CC) $(CCFLAGS) main.c -o main.o

ztx.o: ztx.c ztx.h
	$(CC) $(CCFLAGS) ztx.c -o ztx.o

clean:
	$(RM) $(OBJS)

distclean:
	$(RM) $(OBJS) $(TARGET)

