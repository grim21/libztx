#include <stdio.h>
#include <stdlib.h>
#include <png.h>
#include <zlib.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include "ztx.h"

void draw(GLuint texture_name);
struct ztx_t* load_png(const char* path);

int main(int argc, char* argv[])
{
    int i = 0;
    int length = 0;
    int running = 0;
    struct ztx_t* texture = NULL;
    GLuint tex_id;
    SDL_Window* win = NULL;
    SDL_GLContext ctx = NULL;
    SDL_Event ev;

    if (argc == 1) {
        fprintf(stderr, "Please provide either a PNG or ZTX image.\n");
        return 1;
    } else if (argc == 2) {
        texture = load_png(argv[1]);
        if (texture == NULL) {
            texture = ztx_load_file(argv[1]);
            if (texture == NULL) {
                fprintf(stderr, "Invalid image format.\n");
                return 1;
            }
        }
    } else if (argc == 3) {
        texture = load_png(argv[1]);
        if (texture == NULL) {
            fprintf(stderr, "Can only convert PNG to ZTX, not back to PNG.\n");
            return 1;
        }
        ztx_save_file(texture, argv[2]);
        fprintf(stderr, "Width:           %d\n", texture->width);
        fprintf(stderr, "Height:          %d\n", texture->height);
        fprintf(stderr, "Bytes Per Pixel: %d\n", texture->bpp);
        return 0;
    }

    /* only do all the SDL/OpenGL stuff if the user provides a single file */
    SDL_Init(SDL_INIT_EVERYTHING);
    win = SDL_CreateWindow("Image", SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED, texture->width, texture->height,
        SDL_WINDOW_OPENGL);
    ctx = SDL_GL_CreateContext(win);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glShadeModel(GL_FLAT);
    glEnable(GL_DEPTH_TEST);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glGenTextures(1, &tex_id);
    glBindTexture(GL_TEXTURE_2D, tex_id);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    if (texture->bpp == 4) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture->width,
            texture->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture->pixels);
    } else if (texture->bpp == 3) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture->width, texture->height,
            0, GL_RGB, GL_UNSIGNED_BYTE, texture->pixels);
    }

    running = 1;
    while (running) {
        while (SDL_PollEvent(&ev)) {
            if (ev.type == SDL_QUIT)
                running = 0;
        }

        draw(tex_id);
        SDL_GL_SwapWindow(win);
    }

    SDL_GL_DeleteContext(ctx);
    SDL_DestroyWindow(win);
    SDL_Quit();

    return 0;
}

void draw(GLuint texture_name)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    glBindTexture(GL_TEXTURE_2D, texture_name);
    glColor3f(1.0f, 0.0f, 1.0f);

    glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f);
        glVertex2f(-1.0f, 1.0f);

        glTexCoord2f(1.0f, 1.0f);
        glVertex2f(1.0f, 1.0f);

        glTexCoord2f(1.0f, 0.0f);
        glVertex2f(1.0f, -1.0f);

        glTexCoord2f(0.0f, 0.0f);
        glVertex2f(-1.0f, -1.0f);
    glEnd();
}

/* this function is a perfect illustration of why I don't like libpng.  Rolling
 * your own format is seen as evil, but in this case, I can deal with that */
struct ztx_t* load_png(const char* path)
{
    FILE* fp = NULL;
    int is_png = 0;
    int width, height;
    int color_type, bit_depth;
    int i, j, k, count; 
    struct ztx_t* ret = NULL;

    /* png structures. */
    unsigned char header[8];
    png_bytep* row_pointers = NULL;
    png_byte* row = NULL;
    png_byte* pixel = NULL;
    png_structp png_ptr;
    png_infop info_ptr, end_info;

    fp = fopen(path, "rb");
    if (fp == NULL) {
        return NULL;
    }

    fread(header, 1, 8, fp);
    is_png = !png_sig_cmp(header, 0, 8);
    if (!is_png) {
        fclose(fp);
        return NULL;
    }

    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        fclose(fp);
        return NULL;
    }

    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        png_destroy_read_struct(&png_ptr, NULL, NULL);
        fclose(fp);
        return NULL;
    }

    end_info = png_create_info_struct(png_ptr);
    if (end_info == NULL) {
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        fclose(fp);
        return NULL;
    }

    if (setjmp(png_jmpbuf(png_ptr))) {
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        fclose(fp);
        return NULL;
    }

    png_init_io(png_ptr, fp);
    png_set_sig_bytes(png_ptr, 8);

    png_read_info(png_ptr, info_ptr);
    width = png_get_image_width(png_ptr, info_ptr);
    height = png_get_image_height(png_ptr, info_ptr);
    color_type = png_get_color_type(png_ptr, info_ptr);
    bit_depth = png_get_bit_depth(png_ptr, info_ptr);
    if (bit_depth != 8) {
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        fclose(fp);
        return NULL;
    }

    if (color_type != PNG_COLOR_TYPE_RGB && color_type != PNG_COLOR_TYPE_RGBA) {
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        fclose(fp);
        return NULL;
    }

    row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * height);
    for (i = 0; i < height; ++i) {
        row_pointers[i] = (png_byte*)malloc(
            png_get_rowbytes(png_ptr, info_ptr));
    }

    png_read_image(png_ptr, row_pointers);
    fclose(fp);

    ret = (struct ztx_t*)malloc(sizeof(struct ztx_t));
    if (color_type == PNG_COLOR_TYPE_RGB) {
        ret->pixels = (unsigned char*)malloc(sizeof(unsigned char) *
            width * height * 3);
    } else {    /* then it must be RGBA.  We just checked above.. */
        ret->pixels = (unsigned char*)malloc(sizeof(unsigned char) *
            width * height * 4);
    }

    if (color_type == PNG_COLOR_TYPE_RGB) {
        ret->bpp = 3;
    } else {
        ret->bpp = 4;
    }

    ret->width = width;
    ret->height = height;

    count = 0;
    for (i = height - 1; i >= 0; --i) {
        row = row_pointers[i];
        for (j = 0; j < width; ++j) {
            if (color_type == PNG_COLOR_TYPE_RGB) {
                pixel = &(row[j * 3]);
                for (k = 0; k < 3; ++k) {
                    ret->pixels[count++] = pixel[k];
                }
            } else {
                pixel = &(row[j * 4]);
                for (k = 0; k < 4; ++k) {
                    ret->pixels[count++] = pixel[k];
                }
            }
        }
    }

    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
    return ret;
}

