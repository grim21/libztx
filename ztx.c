#include "ztx.h"

int ztx_free(struct ztx_t* tex)
{
    if (tex == NULL) {
        return ZTX_ERR;
    }

    if (tex->pixels == NULL) {
        free(tex);
        return ZTX_ERR;
    }

    free(tex->pixels);
    free(tex);

    return ZTX_OK;
}

unsigned int ztx_get_buffer_length(struct ztx_t* tex)
{
    if (tex == NULL) {
        return ZTX_ERR;
    }

    return (tex->width * tex->height * tex->bpp) + 20;
}

int ztx_get_pixel_at(struct ztx_t* tex, struct pixel_t* out, int location)
{
    unsigned char r, g, b, a;
    int offset = 0;

    if (tex == NULL) {
        return ZTX_ERR;
    }

    if (tex->pixels == NULL) {
        return ZTX_ERR;
    }

    if (out == NULL) {
        return ZTX_ERR;
    }

    offset = location * tex->bpp;
    r = tex->pixels[offset];
    g = tex->pixels[offset + 1];
    b = tex->pixels[offset + 2];
    if (tex->bpp == 4) {
        a = tex->pixels[offset + 3];
    } else {
        a = 0;
    }

    out->red = r / 255.0f;
    out->green = g / 255.0f;
    out->blue = b / 255.0f;
    out->alpha = a / 255.0f;

    return ZTX_OK;
}

int ztx_get_pixel_xy(struct ztx_t* tex, struct pixel_t* out, int x, int y)
{
    int offset = 0;

    offset = (y * tex->width) + x;
    return ztx_get_pixel_at(tex, out, offset);
}

struct ztx_t* ztx_load_buffer(unsigned char* buffer, unsigned int buff_len)
{
    /* result will hold error codes and random stuff.  com_len is the length
     * of the compressed image data.  i, j and k are indexes for loops.
     * header is used to verify the signature of the file, and stream is used
     * for the decompression of the image using zlib. */
    int result, com_len, i, j, k;
    char header[4];
    z_stream stream;

    /* our final product */
    struct ztx_t* ret = NULL;
    memset(header, 0, 4);       /* erase the header in case it has data */
    memcpy(header, buffer, 4);  /* copy the first four bytes of the buffer */
    result = strncmp(header, ZTX_SIGNATURE, 4);
    if (result != 0) {
        fprintf(stderr, "Not a ZTX File.\n");
        return NULL;
    }

    /* at this point, i'm confident that the buffer holds a ZTX file, so we're
     * going to go ahead and allocate everything escept the pixel data */
    ret = (struct ztx_t*)malloc(sizeof(struct ztx_t));

    /* TODO:  Platform independent code needs to go here, potentially fixed
     * width if I can find a way to figure it out without using stdint.h */

    /* To explain this:  First, you're getting the memory address of the index
     * locations in the buffer.  Once you have those addresses, you cast them
     * to a 32-bit signed int pointer, and after you have that value, you
     * then dereference that casted pointer to obtain the proper value */
    ret->width =  *((int*)&buffer[4]);
    ret->height = *((int*)&buffer[8]);
    ret->bpp =    *((int*)&buffer[12]);
    com_len =     *((int*)&buffer[16]);

    /* allocate enough memory to hold the pixel data. */
    result = ret->width * ret->height * ret->bpp;
    ret->pixels = (unsigned char*)malloc(sizeof(unsigned char) * result);

    /* setting up defaults for Zlib.  No need to make special alloc and free
     * functions.  But if you needed something for that, this is the place you
     * you place it */
    stream.zalloc = Z_NULL;
    stream.zfree = Z_NULL;
    stream.opaque = Z_NULL;

    /* tell zlib where our data is and how much of it there is. */
    stream.next_in = buffer + ZTX_HEAD_LEN;
    stream.avail_in = buff_len - ZTX_HEAD_LEN;
    stream.next_out = ret->pixels;
    stream.avail_out = result;

    /* initialize zlib and inflate the data. */
    result = inflateInit(&stream);
    if (result != Z_OK) {       /* Zlib broke somehow. */
        return NULL;
    }

    result = inflate(&stream, Z_FINISH);
    if (result != Z_STREAM_END) {
        return NULL;            /* failed to decompress data */
    }
    
    /* end the zlib stream and return the decompressed image */
    inflateEnd(&stream);
    return ret;
}

struct ztx_t* ztx_load_file(const char* path)
{
    /* this function essentially loads a file into memory and then passes it
     * into the ztx_load_buffer function. */
    long length = 0;
    unsigned char* data = NULL;
    FILE* fp = NULL;
    struct ztx_t* ret = NULL;

    fp = fopen(path, "rb");
    if (fp == NULL) {
        return NULL;
    }

    /* open the file and seek to the end to determine the length */
    fseek(fp, 0L, SEEK_END);
    length = ftell(fp);
    fseek(fp, 0L, SEEK_SET);

    /* allocate the buffer to hold the data.  read the file and then close it */
    data = (unsigned char*)malloc(sizeof(unsigned char) * length);
    if (data == NULL) {
        fclose(fp);
        return NULL;
    }
    if (fread(data, 1, length, fp) != length) {
        free(data);
        fclose(fp);
        return NULL;
    }
    fclose(fp);

    ret = ztx_load_buffer(data, (unsigned int)length);

    /* important step: freeing the memory */
    free(data);

    return ret;
}

int ztx_save_file(struct ztx_t* tex, const char* path)
{
    int result = 0;
    int index = 0;

    unsigned long int com_len = 0;
    unsigned char* u_data = NULL, * c_data = NULL;

    const char* header = "ZTX!";
    FILE* fp = NULL;

    z_stream stream;

    stream.zalloc = Z_NULL;
    stream.zfree = Z_NULL;
    stream.opaque = Z_NULL;

    result = deflateInit(&stream, Z_BEST_COMPRESSION);
    if (result != Z_OK) {
        deflateEnd(&stream);
        fprintf(stderr, "Failed to initialize Zlib.\n");
        return 1;
    }

    com_len = deflateBound(&stream, tex->width * tex->height * 4);
    c_data = (unsigned char*)malloc(sizeof(unsigned char) * com_len);
    if (c_data == NULL) {
        deflateEnd(&stream);
        free(u_data);
        fprintf(stderr, "Failed to allocate %u bytes of memory.\n", com_len);
        return 1;
    }

    /* clear the memory, just in case */
    memset(c_data, 0, com_len);

    stream.next_in = tex->pixels;
    stream.avail_in = tex->width * tex->height * tex->bpp;
    stream.next_out = c_data;
    stream.avail_out = com_len;

    result = deflate(&stream, Z_FINISH);
    if (result != Z_STREAM_END) {
        deflateEnd(&stream);
        free(c_data);
        fprintf(stderr, "An error occured during compression of %s.\n", path);
        return 1;
    }

    /* will give you how much data was actually compressed to the buffer */
    result = (int)stream.total_out;

    fp = fopen(path, "wb");
    fwrite(header, 1, 4, fp);
    fwrite(&tex->width, 4, 1, fp);
    fwrite(&tex->height, 4, 1, fp);
    fwrite(&tex->bpp, 4, 1, fp);
    fwrite(&result, 4, 1, fp);
    fwrite(c_data, 1, stream.total_out, fp);

    fclose(fp);
    free(c_data);
    deflateEnd(&stream);

    return 0;
}

