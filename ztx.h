#ifndef ZTX_H
#define ZTX_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>

#define ZTX_SIGNATURE       ("ZTX!")
#define ZTX_HEAD_LEN        (20)

/* the two error codes */
#define ZTX_OK              (0)
#define ZTX_ERR             (1)

struct pixel_t {
    float red, green, blue, alpha;
};

struct ztx_t {
    int width, height, bpp;
    unsigned char* pixels;
};

int ztx_free(struct ztx_t* tex);
unsigned int ztx_get_buffer_length(struct ztx_t* tex);
int ztx_get_pixel_at(struct ztx_t* tex, struct pixel_t* out, int location);
int ztx_get_pixel_xy(struct ztx_t* tex, struct pixel_t* out, int x, int y);
struct ztx_t* ztx_load_buffer(unsigned char* buffer, unsigned int buffer_len);
struct ztx_t* ztx_load_file(const char* path);
int ztx_save_file(struct ztx_t* tex, const char* path);

#endif

